Feature: Increase victories and matches of gamer by one

    Scenario: A gamer
        Given A gamer without victories and matches
        When I add 1 victory and match to him
        Then The gamer should have 1 victory and match
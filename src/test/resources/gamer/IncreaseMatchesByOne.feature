Feature: Increase matches of gamer by one

  Scenario: A gamer
    Given A gamer with 10 victories and 10 matches
    When I increase his matches by one
    Then The gamer should have 10 victories and 11 matches
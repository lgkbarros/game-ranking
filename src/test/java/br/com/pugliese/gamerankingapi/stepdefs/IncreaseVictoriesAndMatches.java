package br.com.pugliese.gamerankingapi.stepdefs;

import br.com.pugliese.gamerankingapi.Documents.Gamer;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.assertj.core.api.Assertions.*;

public class IncreaseVictoriesAndMatches {
    private Gamer gamer;

    @Given("^A gamer without victories and matches$")
    public void a_gamer_without_victories_and_matches() throws Throwable {
        gamer = new Gamer("Lucas", "Barros", 0, 0);
    }

    @When("^I add (\\d+) victory and match to him$")
    public void i_add_victory_and_match(int victoriesAndMatches) throws Throwable {
        for (int i = 0; i < victoriesAndMatches; i++) {
            this.gamer.increaseVictoriesAndMatchesByOne();
        }
    }

    @Then("^The gamer should have (\\d+) victory and match$")
    public void the_gamer_should_have_a_specific_victory_and_matches(int victoriesAndMatches) throws Throwable {
        assertThat(this.gamer.getVictories()).isEqualTo(victoriesAndMatches);
        assertThat(this.gamer.getMatches()).isEqualTo(victoriesAndMatches);
    }
}

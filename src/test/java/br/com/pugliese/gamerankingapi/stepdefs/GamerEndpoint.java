package br.com.pugliese.gamerankingapi.stepdefs;

import br.com.pugliese.gamerankingapi.Documents.Gamer;
import br.com.pugliese.gamerankingapi.Repositories.IGamerRepository;
import br.com.pugliese.gamerankingapi.Services.GamerServiceImpl;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GamerEndpoint {

    IGamerRepository gamerRepositoryMock = mock(IGamerRepository.class);

    GamerServiceImpl gamerService;

    List<Gamer> gamers;
    List<Gamer> outputGamers = new ArrayList<Gamer>(Arrays.asList(
            new Gamer("Lucas", "Barros", 10, 11),
            new Gamer("Eduardo", "Moreira", 0, 10)
    ));

    @Before
    public void setup(){
        when(gamerRepositoryMock.findAll()).thenReturn(outputGamers);
        gamerService = new GamerServiceImpl(gamerRepositoryMock);
    }

    @When("The client call getGamers method")
    public void the_client_call_get_gamers_method() throws Throwable {
        gamers = gamerService.findAll();
    }

    @Then("^The client should receive a list with (\\d+) gamers$")
    public void the_client_should_receive_the_list_of_gamers(int countOfGamers) throws Throwable {
        assertThat(gamers.size()).isNotEqualTo(countOfGamers);
    }
}

package br.com.pugliese.gamerankingapi.stepdefs;

import br.com.pugliese.gamerankingapi.Documents.Gamer;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.assertj.core.api.Assertions.assertThat;

public class IncreaseMatchesByOne {
    private Gamer gamer;

    @Given("^A gamer with (\\d+) victories and (\\d+) matches$")
    public void a_gamer_with_victories_and_matches(int victories, int matches) throws Throwable {
        gamer = new Gamer("Lucas", "Barros", victories, matches);
    }

    @When("^I increase his matches by one$")
    public void i_increase_his_matches_by_one() throws Throwable {
        this.gamer.increaseMatchesByOne();
    }

    @Then("^The gamer should have (\\d+) victories and (\\d+) matches$")
    public void the_gamer_should_have_a_specific_victory_and_matches(int victories, int matches) throws Throwable {
        assertThat(this.gamer.getVictories()).isEqualTo(victories);
        assertThat(this.gamer.getMatches()).isEqualTo(matches);
    }
}

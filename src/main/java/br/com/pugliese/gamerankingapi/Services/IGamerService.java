package br.com.pugliese.gamerankingapi.Services;

import br.com.pugliese.gamerankingapi.Documents.Gamer;

import org.springframework.lang.NonNull;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface IGamerService {
    List<Gamer> findAll();
    Gamer findById(String id);
    Gamer save(@NotNull Gamer gamer);
    Gamer increaseVictoriesAndMatchesByOne(@NonNull Gamer gamer);
    Gamer increaseMatchesByOne(@NonNull Gamer gamer);
}

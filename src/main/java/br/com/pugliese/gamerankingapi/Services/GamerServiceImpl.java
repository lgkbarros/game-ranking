package br.com.pugliese.gamerankingapi.Services;

import br.com.pugliese.gamerankingapi.Documents.Gamer;
import br.com.pugliese.gamerankingapi.Repositories.IGamerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
public class GamerServiceImpl implements IGamerService{

    private IGamerRepository repository;

    @Autowired
    public GamerServiceImpl(IGamerRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Gamer> findAll() {
        return repository.findAll(new Sort(Sort.Direction.DESC, "victories"));
    }

    @Override
    public Gamer findById(String id) {
        return repository.findById(id).get();
    }

    @Override
    public Gamer save(@NotNull Gamer gamer) {
        return repository.save(gamer);
    }

    @Override
    public Gamer increaseVictoriesAndMatchesByOne(Gamer gamer) {
        gamer.increaseVictoriesAndMatchesByOne();
        return repository.save(gamer);
    }

    @Override
    public Gamer increaseMatchesByOne(Gamer gamer) {
        gamer.increaseMatchesByOne();
        return repository.save(gamer);
    }
}

package br.com.pugliese.gamerankingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameRankingApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameRankingApiApplication.class, args);
	}
}

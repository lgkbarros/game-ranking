package br.com.pugliese.gamerankingapi.Documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Document(collection = "gamers")
public class Gamer {

    @Id
    private String id;

    @NotEmpty(message = "First name is required")
    private String firstName;

    @NotEmpty(message = "Last name is required")
    private String lastName;

    private int victories;
    private int matches;

    public Gamer(){}

    public Gamer(String firstName, String lastName, int victories, int matches) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.victories = victories;
        this.matches = matches;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getVictories() {
        return victories;
    }

    public void setVictories(int victories) {
        this.victories = victories;
    }

    public int getMatches() {
        return matches;
    }

    public void setMatches(int matches) {
        this.matches = matches;
    }

    public void increaseVictoriesAndMatchesByOne(){
        increaseMatchesByOne();
        this.victories++;
    }

    public void increaseMatchesByOne(){
        this.matches++;
    }

    @Override
    public String toString() {
        return String.format(
                "Gamer[id=%s, firstName='%s', lastName='%s', victories='%d', matches='%d']",
                id, firstName, lastName, victories, matches);
    }
}

package br.com.pugliese.gamerankingapi.Repositories;

import br.com.pugliese.gamerankingapi.Documents.Gamer;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface IGamerRepository extends MongoRepository<Gamer, String> {

}

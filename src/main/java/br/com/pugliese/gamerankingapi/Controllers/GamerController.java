package br.com.pugliese.gamerankingapi.Controllers;

import br.com.pugliese.gamerankingapi.Documents.Gamer;
import br.com.pugliese.gamerankingapi.Services.IGamerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class GamerController {

    @Autowired
    private IGamerService gamerService;

    @CrossOrigin(origins = "*")
    @GetMapping("/gamers")
    public List<Gamer> getAllGamers(){
        return gamerService.findAll();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/gamers")
    public Gamer saveGamer(@Valid @RequestBody Gamer gamer){
        return gamerService.save(gamer);
    }

    @CrossOrigin(origins = "*")
    @PutMapping("/gamers/{id}/increaseVictoriesAndMatchesByOne")
    public Gamer increaseVictoriesAndMatchesByOne(@PathVariable(value = "id", required = true) String id){
        Gamer gamer = gamerService.findById(id);
        return gamerService.increaseVictoriesAndMatchesByOne(gamer);
    }

    @CrossOrigin(origins = "*")
    @PutMapping("/gamers/{id}/increaseMatchesByOne")
    public Gamer increaseMatchesByOne(@PathVariable(value = "id", required = true) String id){
        Gamer gamer = gamerService.findById(id);
        return gamerService.increaseMatchesByOne(gamer);
    }
}
